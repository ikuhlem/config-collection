;;; srefactor-autoloads.el --- automatically extracted autoloads
;;
;;; Code:
(add-to-list 'load-path (directory-file-name (or (file-name-directory #$) (car load-path))))

;;;### (autoloads nil "srefactor" "srefactor.el" (23316 62555 994755
;;;;;;  36000))
;;; Generated autoloads from srefactor.el

(autoload 'srefactor-refactor-at-point "srefactor" "\
Offer contextual menu with actions based on current tag in scope.

Each menu item added returns a token for what type of refactoring
to perform.

\(fn)" t nil)

;;;***

;;;### (autoloads nil nil ("srefactor-lisp.el" "srefactor-pkg.el"
;;;;;;  "srefactor-ui.el") (23316 62556 4755 107000))

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; End:
;;; srefactor-autoloads.el ends here
