(define-package "srefactor" "20170223.540" "A refactoring tool based on Semantic parser framework"
  '((emacs "24.4"))
  :keywords
  '("c" "languages" "tools")
  :url "https://github.com/tuhdo/semantic-refactor")
;; Local Variables:
;; no-byte-compile: t
;; End:
